import React from "react";
//
const TotalAmount = (props) => {
  return (
    <React.Fragment>
      <div
        className="bg-success rounded-right mt-5"
        style={{ maxWidth: "200px" }}
      >
        <p className="m-1 p-2" style={{ color: "#fff", fontSize: "20px" }}>
          TOTAL : {props.total.currency} {props.total.total}
        </p>
      </div>
    </React.Fragment>
  );
};

export default TotalAmount;
