import React, { Component } from "react";
class Product extends Component {
  state = {};
  renderButton(props) {
    console.log(props.remove);
    if (!props.remove) {
      return (
        <button
          className="btn btnHover"
          onClick={() => props.onAdd(props.catalog)}
        >
          Add item to cart
        </button>
      );
    } else {
      return (
        <button
          className="btn btnHover"
          onClick={() => props.onRemove(props.catalog)}
        >
          Remove item from cart
        </button>
      );
    }
  }
  handleStyle() {
    let styleClases = "card m-2 ";
    styleClases += !this.props.remove ? "products" : "";
    return styleClases;
  }
  render() {
    return (
      <React.Fragment>
        <div className={this.handleStyle()}>
          <div className="p-2">
            <img
              src={this.props.catalog.imageURL}
              className="card-img-top"
              alt="..."
            />
            <div className="card-body">
              <h5 className="card-title">{this.props.catalog.name}</h5>
              <p className="card-text">
                {this.props.catalog.currency} {this.props.catalog.price}
              </p>
              {this.renderButton(this.props)}
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default Product;
