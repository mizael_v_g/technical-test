import React, { Component } from "react";
import Product from "./Product";
class CurrentProduct extends Component {
  state = {};
  render() {
    return (
      <React.Fragment>
        <div className="container">
          <div className="row text-center">
            {this.props.catalog.map((catalog) => (
              <Product
                onRemove={this.props.onRemove}
                key={catalog.id}
                catalog={catalog}
                remove={true}
              />
            ))}
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default CurrentProduct;
