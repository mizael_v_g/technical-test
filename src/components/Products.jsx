import React, { Component } from "react";
import Product from "./Product";
class Products extends Component {
  render() {
    return (
      <React.Fragment>
        <div className="container">
          <h1>Product list</h1>
          <div className="row text-center">
            {this.props.catalog.map((catalog) => (
              <Product
                onAdd={this.props.onAdd}
                key={catalog.id}
                catalog={catalog}
                remove={false}
              />
            ))}
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default Products;
