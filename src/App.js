import React, { Component } from "react";
import Products from "./components/Products";
import CurrentProducts from "./components/CurretProducts";
import TotalAmount from "./components/TotalAmount";
class App extends Component {
  state = {
    catalog: [
      {
        id: 1,
        imageURL: "http://placehold.it/150/1b21a0/ffffff",
        name: "product A",
        type: "",
        price: 250,
        currency: "$",
      },
      {
        id: 2,
        imageURL: "http://placehold.it/150/bc4125/ffffff",
        name: "product B",
        type: "",
        price: 350,
        currency: "$",
      },
      {
        id: 3,
        imageURL: "http://placehold.it/150/5abc25/ffffff",
        name: "product C",
        type: "",
        price: 200,
        currency: "$",
      },
      {
        id: 4,
        imageURL: "http://placehold.it/150/ebf742/ffffff",
        name: "product D",
        type: "",
        price: 500,
        currency: "$",
      },
      {
        id: 5,
        imageURL: "http://placehold.it/150/d121c8/ffffff",
        name: "product E",
        type: "",
        price: 100,
        currency: "$",
      },
    ],
    currentProducts: [],
    totalAmount: { total: 0, currency: "$" },
  };
  handleAdd = (product) => {
    const total = this.state.totalAmount.total + product.price;
    const totalAmount = { ...this.state.totalAmount };
    totalAmount.total = total;
    const currentProducts = [...this.state.currentProducts];
    currentProducts.push(product);
    this.setState({ currentProducts, totalAmount });
  };
  handleRemove = (product) => {
    const total = this.state.totalAmount.total - product.price;
    const currentProducts = [...this.state.currentProducts];
    const index = currentProducts.indexOf(product);
    currentProducts.pop(currentProducts[index]);
    const totalAmount = { ...this.state.totalAmount };
    totalAmount.total = total;
    this.setState({ currentProducts, totalAmount });
  };
  render() {
    return (
      <React.Fragment>
        <TotalAmount total={this.state.totalAmount} />
        <CurrentProducts
          catalog={this.state.currentProducts}
          onRemove={this.handleRemove}
        />
        <Products onAdd={this.handleAdd} catalog={this.state.catalog} />
      </React.Fragment>
    );
  }
}

export default App;
