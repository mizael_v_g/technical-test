# Technical Test for Ecomsur

## Project setup

```
yarn install
```

### Compiles and hot-reloads for development

```
yarn start
```

## Bitacora

1. Se detectan componentes:
   1.1 Componente para el listado de productos.
   1.2 Componente para mostrar el producto.
   1.3 Componente para mostar el listado de productos elegidos.
   1.4 Componente para mostar la suma de precio de los productos.

2. Se crea la base para el renderizado de los productos (App.js).
3. Se crean funciones dentro de App.js:
   3.1 handleAdd() = Funcion para añadir producto.
   3.1.1 Esta funcion tambien nos ayuda a sumar el valor del producto a nuesto  
    estado de totalAmount{} asi mismo podemos sobreescribir el valor del estado
   para que nuestra Stateless Functional Component tenga asi un nuevo valor dentro del Real DOM.
   3.2 handleRemove() = Funcion para remover producto.
   3.2.1 Esta funcion tambien nos ayuda a restar el valor del producto a nuesto  
    estado de totalAmount{} asi mismo podemos sobreescribir el valor del estado
   para que nuestra Stateless Functional Component tenga asi un nuevo valor dentro del Real DOM.
4. Se agregan clases de estilo dentro de los tag HTML.
5. Atravez de los componentes pasamos la infromaión requerida como propiedades.
6. Se agregan estilos adicionales a index.css.
